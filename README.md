## How to use it?

Step 1: clone the repository

Step 2: run command in your shell of choice `npm install` within the folder with the project to install required dependencies.

Step 3: run command `npm run start`. Wait for couple moments to see the url listening by the run application. It will be something like http://localhost:8090.

Step 4: open the browser with the url http://localhost:8090/login (employee/employees/project/projects/technology/technologies)
