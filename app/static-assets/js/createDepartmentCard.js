let requestUrl = 'http://localhost:8090/rest/departments';
const neededColumns = ['name', 'description', 'links'];

let request = new XMLHttpRequest();
request.open('GET', requestUrl);
request.responseType = 'json';
request.send();

request.onload = function() {
    let result = request.response;
    let object = result.map(row => {
        let neededValues = {};

        for (let col in row) {
            if (neededColumns.indexOf(col) !== -1) {
                neededValues[col] = row[col];
            }
        }
        return neededValues;
    });

    fillCard(object);
};

function fillCard(jsonObj, num = window.location.search.toString().substr(4)) {
    //console.log();
    const data = jsonObj;
    const objNum = num;
    const technologyData = data[objNum];

    if (!technologyData) {
        throw console.error('no data');
    }

    let body = document.querySelector('.content-paddings-wrapper');

    //создаем заголовок
    appendNode(createNode('h2', `Technology: ${technologyData.name}`), body);
    // секция с описанием
    let descriptionSection = createNode('div', '', 'section');
    appendNode(createNode('h3', 'description'), descriptionSection);
    appendNode(createNode('p', technologyData.description), descriptionSection);
    appendNode(descriptionSection, body);

    //секция с линками
    let linksSection = createNode('div', '', 'section');
    appendNode(createNode('h3', 'Links: '), linksSection);

    //создаем список нод
    let nodeList = createnodeList(technologyData.links);

    //раскидываем ноды по колонкам
    const perColumn = 8;
    createSectionWithColumns(nodeList, perColumn, linksSection);

    appendNode(linksSection, body);
}

function createNode(nodeName, nodeContent, nodeClasses) {
    let node = document.createElement(nodeName);
    if (nodeClasses !== '') {
        node.className = nodeClasses;
    }
    if (nodeContent !== '') {
        node.textContent = nodeContent;
    }
    return node;
}

function appendNode(node, parentNode) {
    parentNode.appendChild(node);
    return parentNode;
}

function createnodeList(data) {
    const innerData = data;
    let nodeList = [];
    innerData.forEach(link => {
        let linkTech = createNode('a', link, 'link');
        linkTech.href = link;
        nodeList.push(
            appendNode(
                appendNode(linkTech, createNode('span', '', 'text')),
                createNode('div', '', 'section-2'),
            ),
        );
    });
    return nodeList;
}

function createSectionWithColumns(list, linkPerColumn, parentNode) {
    let nodeList = list;
    let perColumn = linkPerColumn;
    if (nodeList.length > perColumn) {
        const numColumns = Math.ceil(nodeList.length / perColumn);
        let columnsWrapper = createNode('div', '', 'columns-wrapper');
        for (let i = 0; i < numColumns; i++) {
            let column = createNode('div', '', 'column');
            for (let k = 0; k < perColumn; k++) {
                console.log(nodeList[perColumn * i + k]);
                if (nodeList[perColumn * i + k]) {
                    appendNode(nodeList[perColumn * i + k], column);
                }
            }
            appendNode(column, columnsWrapper);
        }

        appendNode(columnsWrapper, parentNode);
    } else {
        nodeList.forEach(node => {
            appendNode(node, parentNode);
        });
    }
}
