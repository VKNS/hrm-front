function filterFunction(...columnsToFilter) {
    const columnsList = columnsToFilter;
    const inputValue = document
        .getElementById('table-filter')
        .value.toLowerCase();
    const rowsList = document.querySelectorAll('table.table tr.content');

    rowsList.forEach(row => {
        const rowLength = columnsList.length;
        for (let i = 0; i < rowLength; i++) {
            if (
                row.children[columnsList[i]].textContent
                    .toLowerCase()
                    .indexOf(inputValue) > -1 &&
                row.dataset.pagdisplayed === 'true'
            ) {
                row.style.display = 'table-row';
                break;
            } else {
                row.style.display = 'none';
            }
        }
    });
}
