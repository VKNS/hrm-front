const rowsPerPage = 10;
const VisiblePagesRange = 3;

window.onload = function pag() {
    const rowsList = document.querySelectorAll('table.table tr.content');
    const numOfrows = rowsList.length;
    const numOfPages = Math.ceil(numOfrows / rowsPerPage);
    const paginationDiv = document.getElementById('pagination');

    // создаем пагинацию
    const paginationContent = drawPagination(numOfPages);
    paginationDiv.innerHTML = paginationContent;

    // помечаем активную страницу
    let currentPage = document.getElementById('page1');
    currentPage.classList.add('active-page');

    //выводим первые записи

    for (let i = 0; i <= numOfrows; i++) {
        if (i < rowsPerPage) {
            rowsList[i].style.display = 'table-row';
            rowsList[i].dataset.pagdisplayed = 'true';
        }
    }
};

function drawPagination(pages, page = 'page1') {
    const numOfPages = pages;
    const currentPage = +page.substr(4);

    let paginationContent = '';

    // проверка для обрезание количества отображаемых страниц
    if (
        numOfPages > 9 //VisiblePagesRange * 2 + 1
    ) {
        // проверка на минимальное число страниц
        let minPage = currentPage - VisiblePagesRange;
        if (minPage < 1) {
            minPage = 1;
        }
        // проверка на максимальное число страниц
        let maxPage = currentPage + VisiblePagesRange; /*else if (maxPage>=)*/
        if (maxPage >= numOfPages) {
            maxPage = numOfPages;
        }
        // пересчет разницы
        if (currentPage <= VisiblePagesRange) {
            maxPage = 1 + 2 * VisiblePagesRange;
        }
        if (currentPage >= numOfPages - VisiblePagesRange) {
            minPage = numOfPages - 2 * VisiblePagesRange;
        }
        //проверка на добавление левой стрелочки
        /*if (currentPage !== 1) {
            paginationContent =
                paginationContent +
                `<span class="left-arrow""> <i class="material-icons">&#xe5cb;</i></span>`;
        }*/

        if (currentPage > VisiblePagesRange + 1) {
            //проверка для добавления кнопки первой страницы
            paginationContent += `<span data-page=${0} id="page${1}" class="not-visible-on-mobile"> ${1} </span>`;
            paginationContent += `<span  id="not-page" class="not-visible-on-mobile"> ... </span>`;
        }

        for (let i = minPage - 1; i < maxPage; i++) {
            paginationContent += `<span data-page=${i *
                rowsPerPage} id="page${i + 1}"> ${i + 1} </span>`;
        }
        //проверка для добавления кнопки последней страницы
        if (currentPage < numOfPages - VisiblePagesRange - 2) {
            paginationContent += `<span  id="not-page" class="not-visible-on-mobile"> ... </span>`;
            paginationContent += `<span data-page=${rowsPerPage *
                (numOfPages -
                    1)} class="not-visible-on-mobile" id="page${numOfPages}" > ${numOfPages} </span>`;
        }
    } else {
        for (let i = 0; i < numOfPages; i++) {
            paginationContent += `<span data-page=${i *
                rowsPerPage} id="page${i + 1}"> ${i + 1} </span>`;
        }
    }
    //проверка на добавление правой стрелочки
    /*if (currentPage !== numOfPages) {
        paginationContent += `<span class="right-arrow""> <i class="material-icons">chevron_right</span>`;
    }*/

    return paginationContent;
}

/*-------------------------------------------*/
function changePage(event) {
    const e = event;
    const target = e.target;

    if (
        target.tagName.toLowerCase() !== 'span' ||
        target.id.toLowerCase() === 'not-page'
    ) {
        return;
    }

    const rowsList = document.querySelectorAll('table.table tr.content');
    const numOfPages = Math.ceil(rowsList.length / rowsPerPage);
    // определяем целевой айди
    let targetId = target.id;
    /*console.log(e);
    if (document.querySelector('#pagination span.active-page')) {
        const currentPage = document.querySelector(
            '#pagination span.active-page',
        );

        if (target.className === 'left-arrow') {
            console.log(document.querySelector('#pagination span.active-page'));
            targetId = `page${+currentPage.id.substr(4) - 1}`;
        } else if (target.className === 'right-arrow') {
            targetId = `page${+currentPage.id.substr(4) - 1}`;
        }
    }*/

    if (numOfPages > 9) {
        //перерисовка пагинэйшн бара
        const paginationDiv = document.getElementById('pagination');
        let paginationContent = drawPagination(numOfPages, targetId);
        paginationDiv.innerHTML = paginationContent;
    }

    // меняем активную кнопку
    changeActivePage(targetId, numOfPages);

    // меняем показ активных строчек таблицы
    const pageFromData = +target.dataset.page;
    changeContentOnPage(rowsList, rowsPerPage, pageFromData);
}

function changeActivePage(id, pages) {
    const targetId = id;
    const numOfPages = pages;
    if (
        document.querySelector('#pagination span.active-page' !== null) ||
        numOfPages <= 9
    ) {
        let currentPage = document.querySelector(
            '#pagination span.active-page',
        );
        currentPage.className = '';
    }

    let nextPage = document.getElementById(targetId);
    nextPage.classList.add('active-page');
}

function changeContentOnPage(
    objects,
    objectsPerPage,
    lastObjectOnPreviousPage,
) {
    const rowsList = objects;
    const rowsPerPage = objectsPerPage;
    const pageFromData = lastObjectOnPreviousPage;

    // удаление предыдущих страниц
    for (let i = 0; i < rowsList.length; i++) {
        let numFromData = rowsList[i].dataset.number;
        if (
            numFromData <= pageFromData ||
            numFromData > pageFromData + rowsPerPage
        ) {
            rowsList[i].style.display = 'none';
            rowsList[i].dataset.pagdisplayed = 'false';
        }
    }
    // делаем видимой настоящую страницу
    let j = 0;
    for (let i = pageFromData; i <= pageFromData + rowsPerPage; i++) {
        if (j >= rowsPerPage) {
            break;
        }
        if (rowsList[i]) {
            rowsList[i].style.display = 'table-row';
            rowsList[i].dataset.pagdisplayed = 'true';
            j++;
        }
    }
}
