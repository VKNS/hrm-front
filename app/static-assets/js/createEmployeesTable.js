let requestUrl = 'http://localhost:8090/rest/employees';

let request = new XMLHttpRequest();
request.open('GET', requestUrl);
request.responseType = 'json';
request.send();

request.onload = function() {
    let object = request.response;
    fillTable(object);
};

function fillTable(jsonObj) {
    const data = jsonObj;
    let table = document.querySelector('table.table ');
    // создаем хедер таблицы
    let header = document.createElement('tr');
    header.className = 'header';

    let colNum = 0;
    Object.keys(data[0]).forEach(key => {
        let column = document.createElement('th');
        column.className = 'th-header';
        column.dataset.column = colNum;
        colNum++;
        column.textContent = key;
        header.appendChild(column);
    });
    table.appendChild(header);
    // создаем контент таблицы
    let num = 1;
    data.forEach(obj => {
        let row = document.createElement('tr');
        row.className = 'content';
        row.dataset.number = num;
        num++;
        let colNum = 0;
        for (let key in obj) {
            let column = document.createElement('td');
            column.dataset.column = colNum;
            colNum++;
            // проверка на array
            if (Array.isArray(obj[key])) {
                let cellArr = obj[key]
                    .toString()
                    .split(',')
                    .join(',\u000A');
                column.innerText = cellArr;
                //перенос слов
            } else if (obj[key].toString().length > 12) {
                let cellContent = obj[key]
                    .toString()
                    .split(' ')
                    .join(' \u000A');
                column.innerText = cellContent;
            } else {
                column.textContent = obj[key];
            }

            row.appendChild(column);
        }
        table.appendChild(row);
    });
}
