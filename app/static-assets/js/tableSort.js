function sortTable(event) {
    const e = event;
    const target = e.target;
    if (target.className !== 'th-header') {
        return;
    }
    const columnNum = +target.dataset.column;

    //проверка на число и выявление типа сортировки
    let sortType;
    if (
        Number.isInteger(
            Number(
                document.querySelectorAll('table.table tr.content')[1].children[
                    columnNum
                ].innerHTML,
            ),
        ) === true
    ) {
        sortType = 'num';
    } else {
        sortType = 'string';
    }

    let shouldSwitch = true;
    let reverse = false;
    let switchCount = 0;

    while (shouldSwitch === true) {
        let rowsList = document.querySelectorAll('table.table tr.content');
        shouldSwitch = false;

        // выясняем функцию сортировки
        let sortFunc;
        if (sortType === 'num' && reverse === false) {
            sortFunc = sortNum;
        } else if (sortType === 'num' && reverse === true) {
            sortFunc = sortNumReverse;
        } else if (sortType === 'string' && reverse === false) {
            sortFunc = sortString;
        } else if (sortType === 'string' && reverse === true) {
            sortFunc = sortStringReverse;
        }
        //само сравнение
        for (let i = 0; i < rowsList.length - 1; i++) {
            let firstRow = rowsList[+i].children[columnNum].innerHTML;
            let secondRow = rowsList[+i + 1].children[columnNum].innerHTML;

            if (sortFunc(firstRow, secondRow)) {
                rowsList[i].parentNode.insertBefore(
                    rowsList[i + 1],
                    rowsList[i],
                );
                shouldSwitch = true;
                switchCount++;
                break;
            }
        }
        //для второго клика
        if (switchCount === 0 && reverse === false) {
            reverse = true;
            shouldSwitch = true;
        }
    }
}

function sortNum(a, b) {
    const first = +a;
    const second = +b;
    return first > second;
}
function sortNumReverse(a, b) {
    const first = +a;
    const second = +b;
    return second > first;
}

function sortString(a, b) {
    return a.toLowerCase() > b.toLowerCase();
}

function sortStringReverse(a, b) {
    return a.toLowerCase() < b.toLowerCase();
}
